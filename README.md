# Echo des gnous

L'écho des Gnous est l'émission consacrée au logiciel Libre et à la culture libriste diffusée chaque dimanche soir de 19h à 20h sur Radio Campus à Lille (106,6 FM). Ces émissions sont distribuées selon les termes de la licence Creative Commons Paternité - Partage des conditions initiales (CC-by-sa). En alternance chaque semaine la Face A (Les Actualités du monde du libre) et Face B (Une heure, un sujet sur le libre).

## Installation du Wordpress

1. installer un wordpress 
2. inclure le dossier **themechoGnous** dans le dossier themes du wordpress
3. importer le Dump de la BDD 
4. activer le theme **echo des gnous** dans wordpress 


## dependance 

le themes **Echo des Gnous** (enfant)  à besoin du themes **Twenty Sixteen** (parent)  , ne pas le supprimer 


