let tagAudio = document.querySelector("#radio") ; 

// start time 
let startHours = 19 ; 
let startMinute = 42 ; 
let startDateTime = new Date(0, 0, 0, startHours, startMinute) ; 

// end time 
let endHours = 20 ; 
let endMinute = 2 ; 
let endDateTime = new Date(0, 0, 0, endHours, endMinute) ; 

window.setInterval(gestionAffichageAudio, 12000, tagAudio, startDateTime, endDateTime) ; 


// cette fonction devra etre répéter à interval régulier 
function gestionAffichageAudio(tagAudio, startTime, endTime, diffusionDay = 0) 
{
    let timePc = new Date() ; 
    let minuteDayPC = timePc.getUTCHours() * 60 + timePc.getMinutes() ; 
    let minuteDayStart = startTime.getUTCHours() * 60 + startTime.getMinutes() ; 
    let minuteDayEnd = endTime.getUTCHours() * 60 + endTime.getMinutes() ; 
    let currentDay = timePc.getDay() ; 

    

    if(minuteDayPC > minuteDayStart && minuteDayPC < minuteDayEnd && currentDay == diffusionDay)
    {
        // on diffuse, on est dans intervalle de diffusion 

        // suppression class CSS dans la balise tagAudio
        tagAudio.classList.remove("disabledAudio") ; 
        // tagAudio.play() ; 
        return true ; 
    }
    else
    {
        // ajoute de la class CSS dans la balise tagAudio 
        tagAudio.classList.add("disabledAudio") ;  
        tagAudio.pause() ; 
        

        return false ; 
    }
}


// pour ne pas attendre interval de temp au démarrage du script 
gestionAffichageAudio(tagAudio, startDateTime, endDateTime); 

