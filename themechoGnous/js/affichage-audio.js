let tagAudio = document.querySelector("#edg-radio") ; 
let tagMessage = document.querySelector("#edg-message") ; 

if (tagAudio != undefined)  
{
    // start time 
    let startHours = 19 ; 
    let startMinute = 45 ; 
    let startDateTime = new Date(0, 0, 0, startHours, startMinute) ; 
    
    // end time 
    let endHours = 20 ; 
    let endMinute = 2 ; 
    let endDateTime = new Date(0, 0, 0, endHours, endMinute) ; 
    
    window.setInterval(gestionAffichageAudio, 12000, tagAudio, startDateTime, endDateTime) ; 
    
    
    
    // pour ne pas attendre interval de temp au démarrage du script 
     gestionAffichageAudio(tagAudio, startDateTime, endDateTime); 

    // for the test 
    //tagAudio.play() ; 
    
    
}    

// cette fonction devra etre répéter à interval régulier 
function gestionAffichageAudio(tagAudio, startTime, endTime, diffusionDay = 0) 
{
    let timePc = new Date() ; 
    let minuteDayPC = timePc.getUTCHours() * 60 + timePc.getMinutes() ; 
    let minuteDayStart = startTime.getUTCHours() * 60 + startTime.getMinutes() ; 
    let minuteDayEnd = endTime.getUTCHours() * 60 + endTime.getMinutes() ; 
    let currentDay = timePc.getDay() ; 

    

    if(minuteDayPC > minuteDayStart && minuteDayPC < minuteDayEnd && currentDay == diffusionDay)
    {
        // on diffuse, on est dans intervalle de diffusion 

        // suppression class CSS dans la balise tagAudio
        tagAudio.classList.remove("edg-disabledAudio") ; 
        if (tagMessage != undefined)
            tagMessage.classList.add("edg-disabledAudio") ;
        //tagAudio.play() ; 
        return true ; 
    }
    else
    {
        // ajoute de la class CSS dans la balise tagAudio 
        tagAudio.classList.add("edg-disabledAudio") ; 
        if ( tagMessage != undefined)
            tagMessage.classList.remove("edg-disabledAudio") ; 
        tagAudio.pause() ; 
        

        return false ; 
    }
}
